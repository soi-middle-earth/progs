Created: 26 February 2017 by Nimrod
Last Udate: 27 February 2017 by Nimrod

These are the lease progs to rent rooms in SoI.

Simple English Concept.
There are PUBLIC (located outside the house[s] for lease) and PRIVATE (loctaed in the house for lease) objects.  The PUBLIC object is placed in a public area, and allows characters to review what leases are available.  The PUBLIC item will read all directions from its location, searching for an adjacent PRIVATE object, the user is presented with the details of the lease (cost and duration) and may then begin to lease any available space.  Upon lease commencement, the system loads a key to the user and marks it with a timestamp.  The user may then use the key to unlock the door to their newly leased apartment.  Additionally, the PRIVATE object is marked with the user's name and timestamped to record the beginning of the lease. (Note that the timestamp applied to the key is the same value as the timestamp used for the beginning of the lease that is written to the PRIVATE object.)

If a user falls behind on lease payments, they may cancel their lease, giving up their home.  This is allowed to help them avoid additional fines and monthly lease costs that would continue to accrue.  If someone does cancel their lease when they have a fine, that fine will be transferred to the office associated with that lease unit and the user will not be able to lease through that office until they have paid their fine.

The PRIVATE object is used to store all details about the lease (see PRIVATE OBJECT VARIABLES for a list and explanation).  If the lease is expired, the user will be notified when next they attempt to unlock the door.  Notifications will be written to admin vboards when a user is x days overdue on their lease.  If an admin determines that a user must be evicted, they may evict the user and may set a fine.  The find AND the back lease owed MUST be paid before the user may re-enter the space again.  A user may ALWAYS leave the space no matter how far behind they are in their lease.

If an admin wishes to completely evict a user, they may set the FINE extremely high, or simply reset the PRIVATE object.  Resetting the PRIVATE object completely removes all lease information of previous tenant, and old keys no longer function.  The Admin will be required to manually walk through the leased space and purge all contents.

During setup, an admin will drop a PUBLIC object outside the intended units to be leased.  Number of houses, or direction does not matter, the PUBLIC object will detect the PRIVATE objects in any direction.  Once the PUBLIC object is placed, a PRIVATE object is placed in the room adjacent (inside the house to be leased).  There should be a lockable door between the PRIVATE and PUBLIC objects.  The PRIVATE object is then configured by the Admin via the following commands LEASESET KEY <key vnum>, LEASESET COST <integer>, LEASESET TIME <integer>, LEASESET OBJECT <vnum>.  PUBLIC objects are configured with these commands: LEASECONFIG AUTOEVICT <days>, LEASECONFIG AUTOFINE <integer>, LEASECONFIG AUTO, LEASECONFIG OFFICE <vnum>, LEASECONFIG MASTER <key vnum>.  This concludes setup for LEASE units. Please note that all AUTO services apply to all lease units adjacent to the PUBLIC object.  i.e. AUTO settings are set on the PUBLIC object and apply to all PRIVATE objects in adjacent rooms.

When a user wishes to lease a house, they may perform the following commands.  LEASE PREVIEW <direction>, LEASE UNIT <direction>, LEASE RENEW, LEASE REQUEST KEY, PAY FINE <direction>, PAY FINE OFFICE, CANCEL LEASE <direction>, CHANGE LOCK <direction>.

Admins may also use the following commands: LEASESET EVICT <fine>, & LEASESET RESET.

All user commands, except preview and unlock, are written to our lease vboard and player board (if set).  Admin commands are not currently tracked.

When a lease runs overdue, a message is sent to the player each time they access their domicile.  Access is automatically revoked after the set number of days in AUTOEVICT.  If LEASESET OFFICE is set, a message is written to a player board in that room as well.

Variables:
read room number of house with info from direction entered.

PRIVATE OBJECT VARIABLES:
lease_key ..... oval0 .... (vnum of key to use for this rental) needs to be unique for each lease unit.  
lease_cost .... oval1 .... (cost per lease_time)  default:30cp
lease_time .... oval2 .... (number of days that lease_cost covers) default:30 days
lease_end ..... oval3 .... (timestamp of when lease is done)
lease_began ... oval4 .... (timestamp of when lease was started by leasee
lease_obj ..... oval5 .... (this object is required to lease or pay on lease)
lease_payers .. cost ..... (if 0, then only leasee may pay lease and see overdue messages.  If >0 then keyholder can pay and see messages)
lease_fine .... spare .... (fines levied by going late or whatever else.  If there's a positive balance here, no one can enter until it's paid.)
lease_holder .. taste .... (writes name of person leasing thus: "xxxNAMExxx"
public_room ... quality .. (room vnum where associated public_object is)

PRIVATE OBJECT PROGS:
LEASESET evict &(fine) - sets lease_end to &(fine) (less than 100000) will not allow lease holder to enter without being current and paying fine.  Fine is value of lease_end. ADMIN ONLY
LEASESET key, PROG2 ADMIN ONLY - sets value of lease_key
LEASESET cost, PROG2 ADMIN ONLY - sets value of lease_cost
LEASESET time, PROG2 ADMIN ONLY - sets value of lease_time
LEASESET object, PROG2 ADMIN ONLY - sets value of lease_obj
LEASESET info, PROG2 ADMIN ONLY - Displays all information o.to fine amount (cannot be greater than 99,999.
UNLOCK door ne - PROG4 - chekcs if okay, doitanyway (unlocks door)  if overdue, or evicted, gives notice, but always allows the keyholder to leave.  (Maybe just trans them outside without unlocking door)  Perhaps allow anyone trapped inside an emergency way outside?

--------------------------------------------------------

PUBLIC OBJECT VARIABLES:
autoevict		oval0		(days overdue before tenant is auto-evicted)
autofine		oval1		(fine applied after # of days in autoevict)
auto			oval2		(CAN'T REMEMBER WHAT THIS IS FOR)
office			oval3		(vnum of room where LEASE OFFICE is located)
master			oval4		(vnum of master key) Allows holder to evict and manage rentals.
keycost			oval5		(cost for a new key, rekey is 2X new key cost)

- A single Public Object will be placed outside the rental units.
- Public Object will cover rental units in all directions.
- unlock is trapped by Public object (checks Lease Object inside rental to see if we should unlock door)

PUBLIC OBJECT PROGS:
LEASE PREVIEW <direction>
LEASE UNIT <direction> - checks if available, takes money, takes item, loads key (sets timestamp on key)
UNLOCK DOOR <direction> - catches unlock command at door, reads direction, reads x_target from door object, reads lease_days from lease object.  
	If lease_days are less than 0, a warning message is shown to unlocker showing how many days overdue, and system message is sent.
LEASE renew &(direction) - player can renew lease from outside the house, updates lease_end timestamp.  (needs to be outside in case they've been evicted)
NEWKEY <dir> SHOULD BE ON PUBLIC OBJECT so we can read keycost
REKEY <dir> SHOULD BE ON PUBLIC OBJECT so we can read keycost
EVICT <direction> <fine> COMMIT - sets unit to evicted
LEASECONFIG AUTOEVICT <days>
LEASECONFIG AUTOFINE <amount>
LEASECONFIG AUTO -  Can't remember what this was for.
LEASECONFIG OFFICE <vnum>
LEASECONFIG MASTER <key vnum>
LEASECONFIG KEYCOST <cost>
--------------------------------------------------------
LESSEE OBJECT: 
LESSEE objects are loaded in the referenced office PUBLIC->OFFICE.  LESSEE objects keep track of how many leases an individual currently has, how many leases they are allowed to have at any one time, and any outstanding fines that they may have.

LESSEE OBJECT VARIABLES:
lessee_count... oval0 .... Number of leases this character has.  
lessee_max .... oval1 .... Maximum number of leases a character can have out of this office.
lessee_fines .. oval2 .... Outstanding fines.  (how to combine with the fines on the private_object?)
lessee_ ....... oval3 .... 
lessee_ ....... oval4 .... 
lessee_ ....... oval5 .... 
lessee_ ....... cost ..... 
lessee ........ spare .... 
lessee_name ... taste .... Name of character on the lease.
lessee_ ....... quality .. 



OFFICE OBJECT VARIABLES:
office_max ..... oval0 ..... Default maximum number of leases a peson can make out of this office.
office_

OFFICE OBJECT PROGS:


MASTERKEY OBJECT VARIABLES: (not yet implemented)
MASTERKEY OBJECT PROGS: (not yet implemented)
