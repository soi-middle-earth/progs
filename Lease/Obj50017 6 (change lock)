opprg 50017 6
// Obj 50017 6 (change lock)
// Created by: Nimrod 1 Mar 2017
// Last modified by: Nimrod 7 Mar 2017 8:54
// Last uploaded by: Nimrod 7 Mar 2017 18:48
set var version Version 0.0.0.5
// Read direction and find target_room vnum
if admin()
  set var debug_output &(3)
else do
  set var debug_output disabled
fi done
set var public_object 50017
set var private_object 50016
set var currency coppers
set var arg_one &(1)
set var arg_two &(2)
if defined(arg_one)
  // Arg one is defined, allow them to pass.
else do
  vbr -
  vstr Syntax: #6CHANGE LOCK <dir>#0
  vbr -
  halt -
fi done
// grab target_room based on direction used in command (arg_one)
info target_room room -1 exit &(arg_one) vnum
info lease_key room &(target_room) obj &(private_object) oval0
info lease_cost room &(target_room) obj &(private_object) oval1
info lease_time room &(target_room) obj &(private_object) oval2
info lease_end room &(target_room) obj &(private_object) oval3
info lease_began room &(target_room) obj &(private_object) oval4
info lease_obj room &(target_room) obj &(private_object) oval5
info lease_payers room &(target_room) obj &(private_object) cost
info lease_holder room &(target_room) obj &(private_object) taste
info lease_fine room &(target_room) obj &(private_object) spare
info user_name room -1 char -1 name
info held_key_sn room -1 char -1 inv &(lease_key) oval5
info key_cost room -1 obj &(public_object) oval5
set var rekey_cost &(key_cost)
math rekey_cost multiply 2
if (&(debug_output)=debug)
  vstr target_room: &(target_room) * lease_began: &(lease_began) * required_key &(lease_key)
  vstr held: &(held) * lease_fine: &(lease_fine) * lease_end: &(lease_end)
  vstr key_sn: &(key_sn) * lease_payers: &(lease_payers) * lease_holder: &(lease_holder)
  vstr lease_began: &(lease_began) * user_name: &(user_name) * public_object: &(public_object)
  vstr private_object: &(private_object) * overdue_days: &(overdue_days)* rekey_cost: &(rekey_cost)
  vstr version: &(version)
fi done
if (&(target_room)>0)
  // Allow them to continue
else do
  vbr -
  vstr Syntax: #6CHANGE LOCK <dir>#0
  vbr -
  halt -
fi done
if oexist(&(private_object),&(target_room))
  // vstr The private object exists in &(target_room), continue.
else do
  vstr There is no lease in that direction.
  vbr -
  halt -
fi done
if (&(user_name)=&(lease_holder))
  // vstr It's the lease holder, let them pass.
else do
  // Keyholders cannot request to change the locks, only the lease_holder
  vstr Trying to change the locks on a lease that you don't hold?  Sneaky.
  vbr -
  halt -
fi done
if (&(2)=&(rekey_cost))
  // They've entered the correct cost in the command.
else do
  vbr -
  vstr The cost to change the locks is &(rekey_cost) &(currency).  You must acknowledge this cost thus:
  vstr Syntax #6CHANGE LOCK &(1) &(rekey_cost)#0
  vbr -
  vstr #BNOTE:#0 You will need to purchase a new key after you change the locks.
  vstr Syntax to purchase new key: #6REQUEST KEY <dir>#0. Cost is &(key_cost) &(currency).
  halt -
fi done
if (&(lease_end)>&(timestamp))
  vstr You are behind on lease payments.  Please make a payment on your lease before you change the locks.
  vstr Syntaxt to pay lease: #6PAY LEASE <dir> <amount>#0
  vbr -
  halt -
fi done
if (&(lease_fine)>0)
  vstr There is a fine of &(lease_fine) that must be paid before you can change the locks.
  vstr Syntax to pay fine: #6PAY FINE &(1) &(lease_fine)#0
  vbr -
  halt -
fi done
// Everything's okay, let's take their money if we can.
if can_take_money(&(rekey_cost), chips)
  takemoney -1 &(rekey_cost) chips 
else do
  vstr You don't have enough coin to change the locks, come back when you have more.
  vbr -
  halt -
fi done
// Looks good, let's modify the lease_began timestamp.
math lease_began sub 1
setval room &(target_room) obj &(private_object) oval4 &(lease_began)
// Load the key to their hand and set serial number
vstr The locks have been changed.  You will have to request a new key.
vstr Syntax: #6REQUEST KEY <dir>#0
atwrite !leases "&(user_name) changed the locks at &(target_room)" &(user_name) just changed the locks at &(target_room). Cost: &(rekey_cost).
@